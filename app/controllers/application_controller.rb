class ApplicationController < ActionController::Base
  @secret = "test"
  
  def show
    @article = Article.find(params[:id])
  end
end
